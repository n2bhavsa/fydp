#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <errno.h>
#include <bluetooth/rfcomm.h>
#include <bluetooth/l2cap.h>

#include <stdbool.h>


int main(int argc, char ** argv){
	
	inquiry_info *detectedDevices;
	int max_rsp, num_rsp;
	int localBluetoothAdapter, sock, discoveryTime, flags, clientSock;

	/**
	 * localBluetoothAdapter: Local Bluetooth adapter resource number
	 * 
	 */

	char addr[19] = {0};
	char name[248] = {0};

	/**
	 * Retrieve the resource number for the first available local 
	 * Bluetooth adapter
	 */
	localBluetoothAdapter = hci_get_route(NULL);

	/**
	 * Allocate resources to use chosen adapter
	 */
	sock = hci_open_dev(localBluetoothAdapter);

	if(localBluetoothAdapter < 0 || sock < 0){
		fprintf(stderr, "error code %d: %s\n", errno, strerror(errno));
		exit(1);
	}
	
	discoveryTime = 5; //hci_inquiry lasts for 1.26 * discoveryTime (s)
	max_rsp = 255; 
	flags = IREQ_CACHE_FLUSH; //Cache of previously detected devices cleared
	detectedDevices = (inquiry_info*)malloc(max_rsp * sizeof(inquiry_info));

	/**
	 * Perform Bluetooth discovery
	 */
	num_rsp = hci_inquiry(localBluetoothAdapter, 
		discoveryTime, max_rsp, NULL, &detectedDevices, flags);

	if(num_rsp < 0) {
		fprintf(stderr, "hci_inquiry; error code %d: %s\n", errno, strerror(errno));
		exit(1);
	}

	//Go over all detected devices and print out friendly-name
	for(int i = 0; i < num_rsp; i++){
		//Convert from bdaddr_t to char*
		ba2str(&(detectedDevices+i)->bdaddr, addr);

		memset(name, 0, sizeof(name));

		if(hci_read_remote_name(sock, &(detectedDevices+i)->bdaddr, 
			sizeof(name),name, 0) < 0){
				strcpy(name, "[unknown]");
			}
		printf("%i: %s, %s\n", i, addr, name);

	}
	
	int userDevice;
	if(num_rsp > 0){
		printf("Which device would you like to pair to: \n");
		scanf("%i", &userDevice);
		ba2str(&(detectedDevices+userDevice)->bdaddr, addr);
		printf("Pairing to: %s\n", addr);
	}
	else
		printf("No Bluetooth-enabled devices found\n");	

	close(sock);

	//Open a L2CAP socket
	sock = socket(AF_BLUETOOTH, SOCK_SEQPACKET, BTPROTO_L2CAP);
	struct sockaddr_l2 local_addr, client_addr;
	int opt = sizeof(client_addr);
	char buffer[1024];
	bool wrongConnection = false;

	local_addr.l2_family = AF_BLUETOOTH;
	local_addr.l2_bdaddr = *BDADDR_ANY;
	local_addr.l2_psm = htobs(0x1001);

	fprintf(stderr, "Binding new socket\n");

	bind(sock, (struct sockaddr *)&local_addr, sizeof(local_addr));

	do{
		fprintf(stderr, "Listening on new socket\n");
		listen(sock, 2);
		clientSock = accept(sock, (struct sockaddr *)&client_addr, &opt);
		ba2str( &client_addr.l2_bdaddr, buffer);
		
		wrongConnection = !(strcmp(buffer, addr) == 0);

		if(wrongConnection){
			fprintf(stderr, "Incorrect connection, closing.\n");
			close(clientSock);
		}
	}while(wrongConnection);
	
	fprintf(stderr, "accepted connection from %s\n", buffer);

	close(sock);
	close(clientSock);
	free(detectedDevices);
}
