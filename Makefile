
CXX=gcc
CXXFLAGS= -lbluetooth
DEPS = 
OBJ = scan.o

%.o: %.c $(DEPS)
		$(CXX) -c -o $@ $< $(CXXFLAGS)

default: scan.o 
		$(CXX) -o scan scan.o $(CXXFLAGS) 
clean: 
	rm -f $(OBJ) scan
cleanall:
	rm -f $(OBJ) $(CSV) scan

