/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * File Name          : App/p2p_server_app.c
 * Description        : P2P Server Application
 ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "app_common.h"
#include "dbg_trace.h"
#include "ble.h"
#include "p2p_server_app.h"
#include "stm32_seq.h"
#include "app_entry.h"
#include "hw_conf.h"
#include "otp.h"
#include "stm32_lpm.h"
#include "stm32_seq.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
#if((BLE_CFG_HIDS_INPUT_REPORT_NB != 0) || (BLE_CFG_HIDS_KEYBOARD_DEVICE != 0) || (BLE_CFG_HIDS_MOUSE_DEVICE != 0))
typedef struct
{
#if(BLE_CFG_HIDS_INPUT_REPORT_NB != 0)
  uint8_t ReportNotificationEnabled;
  uint8_t TimerTracking_Id;
  uint8_t TimerScrolling_Id;
#endif
#if(BLE_CFG_HIDS_KEYBOARD_DEVICE != 0)
  uint8_t KeyboardInputNotificationEnabled;
#endif
#if(BLE_CFG_HIDS_MOUSE_DEVICE != 0)
  uint8_t MouseInputNotificationEnabled;
#endif
} HIDSAPP_Context_t;
#endif

typedef struct
{
  uint8_t buttons;
  int8_t x;
  int8_t y;
  int8_t wheel;
} mouse_report_t;

typedef struct
{
	int isPressed;
	uint32_t pressedTimestamp;
	uint32_t releasedTimestamp;
} button_info_t;
/* USER CODE END PTD */

/* Private defines ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define MOUSE_REPORT_SIZE       52

#define ENABLED         1
#define DISABLED        0

#define HIDSAPP_TRACKING_INTERVAL		(50000/CFG_TS_TICK_VAL)  /* 50ms */
#define HIDSAPP_SCROLLING_INTERVAL		(300000/CFG_TS_TICK_VAL)  /* 300ms */

#define HIDSAPP_DEBOUNCE_THRESHOLD		(20000/CFG_TS_TICK_VAL)  /* 20ms */
#define HIDSAPP_SINGLE_CLICK_THRESHOLD	(100000/CFG_TS_TICK_VAL)  /* 100ms */
/* USER CODE END PD */

/* Private macros -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */
static uint8_t report_mouse[MOUSE_REPORT_SIZE] =
{
  0x05, 0x01,         /* USAGE_PAGE (Generic Desktop) */
  0x09, 0x02,         /* USAGE (Mouse) */
  0xa1, 0x01,         /* COLLECTION (Application) */
  0x09, 0x01,         /*   USAGE (Pointer) */
  0xa1, 0x00,         /*   COLLECTION (Physical) */
  0x05, 0x09,         /*     USAGE_PAGE (Button) */
  0x19, 0x01,         /*     USAGE_MINIMUM (Button 1) */
  0x29, 0x03,         /*     USAGE_MAXIMUM (Button 3) */
  0x15, 0x00,         /*     LOGICAL_MINIMUM (0) */
  0x25, 0x01,         /*     LOGICAL_MAXIMUM (1) */
  0x95, 0x03,         /*     REPORT_COUNT (3) */
  0x75, 0x01,         /*     REPORT_SIZE (1) */
  0x81, 0x02,         /*     INPUT (Data,Var,Abs) */
  0x95, 0x01,         /*     REPORT_COUNT (1) */
  0x75, 0x05,         /*     REPORT_SIZE (5) */
  0x81, 0x03,         /*     INPUT (Cnst,Var,Abs) */
  0x05, 0x01,         /*     USAGE_PAGE (Generic Desktop) */
  0x09, 0x30,         /*     USAGE (X) */
  0x09, 0x31,         /*     USAGE (Y) */
  0x09, 0x38,         /*     USAGE (Wheel) */
  0x15, 0x81,         /*     LOGICAL_MINIMUM (-127) */
  0x25, 0x7f,         /*     LOGICAL_MAXIMUM (127) */
  0x75, 0x08,         /*     REPORT_SIZE (8) */
  0x95, 0x03,         /*     REPORT_COUNT (3) */
  0x81, 0x06,         /*     INPUT (Data,Var,Rel) */
  0xc0,               /*   END_COLLECTION (Physical) */
  0xc0,               /* END_COLLECTION (Application) */
};

#if((BLE_CFG_HIDS_INPUT_REPORT_NB != 0) || (BLE_CFG_HIDS_KEYBOARD_DEVICE != 0) || (BLE_CFG_HIDS_MOUSE_DEVICE != 0))
PLACE_IN_SECTION("BLE_APP_CONTEXT") HIDSAPP_Context_t HIDSAPP_Context;
#endif

volatile int isScrolling = 0;
volatile int isTracking = 0;

uint8_t leftClick = 0;
uint8_t rightClick = 0;

button_info_t middleButtonInfo = {0, 0, 0};
int wasScrolling = 0;

uint32_t lastSW1Change = 0;
uint32_t lastSW2Change = 0;
uint32_t lastSW3Change = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */
static void trackMouse( void );
static void scrollMouse( void );
static void HIDSAPP_Tracking_UpdateChar(void);
static void HIDSAPP_Scrolling_UpdateChar(void);
static void HIDSAPP_Change_SW1(void);
static void HIDSAPP_Change_SW2(void);
static void HIDSAPP_Change_SW3(void);

static void HIDSAPP_Change_Tracking(void);
static void HIDSAPP_Change_Scrolling(void);

//extern int16_t acc_x_output , acc_y_output, acc_z_output;

/* USER CODE END PFP */
static void tx_com(uint8_t *tx_buffer, uint16_t len)
{
  #ifdef NUCLEO_F411RE_X_NUCLEO_IKS01A2
  HAL_UART_Transmit(&huart1, tx_buffer, len, 1000);
  #endif
  #ifdef STEVAL_MKI109V3
  CDC_Transmit_FS(tx_buffer, len);
  #endif
}
/* Functions Definition ------------------------------------------------------*/
void P2PS_STM_App_Notification(P2PS_STM_App_Notification_evt_t *pNotification)
{
/* USER CODE BEGIN P2PS_STM_App_Notification_1 */

/* USER CODE END P2PS_STM_App_Notification_1 */
  switch(pNotification->P2P_Evt_Opcode)
  {
/* USER CODE BEGIN P2PS_STM_App_Notification_P2P_Evt_Opcode */

/* USER CODE END P2PS_STM_App_Notification_P2P_Evt_Opcode */

    case P2PS_STM__NOTIFY_ENABLED_EVT:
/* USER CODE BEGIN P2PS_STM__NOTIFY_ENABLED_EVT */

/* USER CODE END P2PS_STM__NOTIFY_ENABLED_EVT */
      break;

    case P2PS_STM_NOTIFY_DISABLED_EVT:
/* USER CODE BEGIN P2PS_STM_NOTIFY_DISABLED_EVT */

/* USER CODE END P2PS_STM_NOTIFY_DISABLED_EVT */
      break;
      
    case P2PS_STM_WRITE_EVT:
/* USER CODE BEGIN P2PS_STM_WRITE_EVT */

/* USER CODE END P2PS_STM_WRITE_EVT */
      break;

    default:
/* USER CODE BEGIN P2PS_STM_App_Notification_default */

/* USER CODE END P2PS_STM_App_Notification_default */
      break;
  }
/* USER CODE BEGIN P2PS_STM_App_Notification_2 */

/* USER CODE END P2PS_STM_App_Notification_2 */
  return;
}

void P2PS_APP_Notification(P2PS_APP_ConnHandle_Not_evt_t *pNotification)
{
/* USER CODE BEGIN P2PS_APP_Notification_1 */

/* USER CODE END P2PS_APP_Notification_1 */
  switch(pNotification->P2P_Evt_Opcode)
  {
/* USER CODE BEGIN P2PS_APP_Notification_P2P_Evt_Opcode */

/* USER CODE END P2PS_APP_Notification_P2P_Evt_Opcode */
  case PEER_CONN_HANDLE_EVT :
/* USER CODE BEGIN PEER_CONN_HANDLE_EVT */

/* USER CODE END PEER_CONN_HANDLE_EVT */
    break;

    case PEER_DISCON_HANDLE_EVT :
/* USER CODE BEGIN PEER_DISCON_HANDLE_EVT */

/* USER CODE END PEER_DISCON_HANDLE_EVT */
    break;
    
    default:
/* USER CODE BEGIN P2PS_APP_Notification_default */

/* USER CODE END P2PS_APP_Notification_default */
      break;
  }
/* USER CODE BEGIN P2PS_APP_Notification_2 */

/* USER CODE END P2PS_APP_Notification_2 */
  return;
}

void P2PS_APP_Init(void)
{
/* USER CODE BEGIN P2PS_APP_Init */
  tBleStatus result = BLE_STATUS_INVALID_PARAMS;

  UTIL_SEQ_RegTask( 1<< CFG_TASK_HID_TRACK_MOUSE_ID, UTIL_SEQ_RFU, HIDSAPP_Tracking_UpdateChar );
  UTIL_SEQ_RegTask( 1<< CFG_TASK_HID_SCROLL_MOUSE_ID, UTIL_SEQ_RFU, HIDSAPP_Scrolling_UpdateChar );
  UTIL_SEQ_RegTask( 1<< CFG_TASK_HID_CHANGE_SW1_ID, UTIL_SEQ_RFU, HIDSAPP_Change_SW1 );
  UTIL_SEQ_RegTask( 1<< CFG_TASK_HID_CHANGE_SW2_ID, UTIL_SEQ_RFU, HIDSAPP_Change_SW2 );
  UTIL_SEQ_RegTask( 1<< CFG_TASK_HID_CHANGE_SW3_ID, UTIL_SEQ_RFU, HIDSAPP_Change_SW3 );

  result = HIDS_Update_Char(REPORT_MAP_CHAR_UUID,
							0,
							0,
							MOUSE_REPORT_SIZE,
							(uint8_t *)&report_mouse);

  if( result == BLE_STATUS_SUCCESS )
  {
	BLE_DBG_APP_MSG("Report Map Successfully Sent\n");
  }
  else
  {
	BLE_DBG_APP_MSG("Sending of Report Map Failed error 0x%X\n", result);
  }

  /**
  * Create timer for tracking mouse position
  */
  HW_TS_Create(CFG_TIM_PROC_ID_ISR, &(HIDSAPP_Context.TimerTracking_Id), hw_ts_Repeated, trackMouse);
  HW_TS_Create(CFG_TIM_PROC_ID_ISR, &(HIDSAPP_Context.TimerScrolling_Id), hw_ts_Repeated, scrollMouse);
/* USER CODE END P2PS_APP_Init */
  return;
}

/* USER CODE BEGIN FD */
void HIDS_Notification(HIDS_App_Notification_evt_t *pNotification)
{
  switch(pNotification->HIDS_Evt_Opcode)
  {

#if(BLE_CFG_HIDS_INPUT_REPORT_NB != 0)
    case HIDS_REPORT_NOTIFICATION_ENABLED:
      {
        BLE_DBG_APP_MSG("HIDS_REPORT_NOTIFICATION_ENABLED\n");
        HIDSAPP_Context.ReportNotificationEnabled = ENABLED;
      }
      break;

    case HIDS_REPORT_NOTIFICATION_DISABLED:
      {
        BLE_DBG_APP_MSG("HIDS_REPORT_NOTIFICATION_DISABLED\n");
        HIDSAPP_Context.ReportNotificationEnabled = DISABLED;
      }
      break;
#endif

#if(BLE_CFG_HIDS_KEYBOARD_DEVICE != 0)
    case HIDS_KEYB_INPUT_NOTIFY_ENABLED:
      {
        BLE_DBG_APP_MSG("HIDS_KEYB_INPUT_NOTIFY_ENABLED\n");
        HIDSAPP_Context[pNotification->Index].KeyboardInputNotificationEnabled = ENABLED;
      }
      break;

    case HIDS_KEYB_INPUT_NOTIFY_DISABLED:
      {
        BLE_DBG_APP_MSG("HIDS_KEYB_INPUT_NOTIFY_DISABLED\n");
        HIDSAPP_Context[pNotification->Index].KeyboardInputNotificationEnabled = DISABLED;
      }
      break;
#endif

#if(BLE_CFG_HIDS_MOUSE_DEVICE != 0)
    case HIDS_MOUSE_INPUT_NOTIFY_ENABLED:
      {
        BLE_DBG_APP_MSG("HIDS_MOUSE_INPUT_NOTIFY_ENABLED\n");
        HIDSAPP_Context[pNotification->Index].MouseInputNotificationEnabled = ENABLED;
      }
      break;

    case HIDS_MOUSE_INPUT_NOTIFY_DISABLED:
      {
        BLE_DBG_APP_MSG("HIDS_MOUSE_INPUT_NOTIFY_DISABLED\n");
        HIDSAPP_Context[pNotification->Index].MouseInputNotificationEnabled = DISABLED;
      }
      break;
#endif

#if(BLE_CFG_HIDS_REPORT_CHAR != 0)
    case HIDS_OUTPUT_REPORT:
      {
        uint8_t i;

        BLE_DBG_APP_MSG("HIDS_OUTPUT_REPORT\n");
        BLE_DBG_HIDS_MSG("HID Intance %d Report %d \n",
                          pNotification->Instance,
                          pNotification->Index);

        for(i = 0; i < pNotification->ReportLength; i++)
          BLE_DBG_HIDS_MSG("Report[%d] 0x%X \n",
                           i,
                           pNotification->pReport[i]);
      }
      break;
#endif

#if((BLE_CFG_HIDS_MOUSE_DEVICE != 0) && (BLE_CFG_HIDS_MOUSE_INPUT_WRITE != 0))
    case HIDS_MOUSE_INPUT_REPORT:
      {
        uint8_t i;

        BLE_DBG_APP_MSG("HIDS_MOUSE_INPUT_REPORT\n");
        BLE_DBG_HIDS_MSG("HID Intance %d Report %d \n",
                          pNotification->Instance,
                          pNotification->Index);

        for(i = 0; i < pNotification->ReportLength; i++)
          BLE_DBG_HIDS_MSG("Report[%d] 0x%X \n",
                           i,
                           pNotification->pReport[i]);
      }
      break;
#endif

#if((BLE_CFG_HIDS_KEYBOARD_DEVICE != 0) && (BLE_CFG_HIDS_KEYBOARD_INPUT_WRITE != 0))
    case HIDS_KEYBOARD_INPUT_REPORT:
      {
        uint8_t i;

        BLE_DBG_APP_MSG("HIDS_KEYBOARD_INPUT_REPORT\n");
        BLE_DBG_HIDS_MSG("HID Intance %d Report %d \n",
                          pNotification->Instance,
                          pNotification->Index);

        for(i = 0; i < pNotification->ReportLength; i++)
          BLE_DBG_HIDS_MSG("Report[%d] 0x%X \n",
                           i,
                           pNotification->pReport[i]);
      }
      break;

    case HIDS_KEYBOARD_OUTPUT_REPORT:
      {
        uint8_t i;

        BLE_DBG_APP_MSG("HIDS_KEYBOARD_OUTPUT_REPORT\n");
        BLE_DBG_HIDS_MSG("HID Intance %d Report %d \n",
                          pNotification->Instance,
                          pNotification->Index);

        for(i = 0; i < pNotification->ReportLength; i++)
          BLE_DBG_HIDS_MSG("Report[%d] 0x%X \n",
                           i,
                           pNotification->pReport[i]);
      }
      break;
#endif

    default:
      break;
  }

  return;
}

static void trackMouse( void )
{
  /**
   * The code shall be executed in the background as aci command may be sent
   * The background is the only place where the application can make sure a new aci command
   * is not sent if there is a pending one
   */
	UTIL_SEQ_SetTask( 1<<CFG_TASK_HID_TRACK_MOUSE_ID, CFG_SCH_PRIO_0);

  return;
}

static void scrollMouse( void )
{
  /**
   * The code shall be executed in the background as aci command may be sent
   * The background is the only place where the application can make sure a new aci command
   * is not sent if there is a pending one
   */
	UTIL_SEQ_SetTask( 1<<CFG_TASK_HID_SCROLL_MOUSE_ID, CFG_SCH_PRIO_0);

  return;
}


/**
 * @brief  Alert Notification Application service update characteristic
 * @param  None
 * @retval None
 */
static uint8_t tx_buffer[1000];
static void tx_com( uint8_t *tx_buffer, uint16_t len );

static void HIDSAPP_Tracking_UpdateChar(void)
{
	tap = 0;
	HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_5);
	mouse_report_t mouse_report;
	uint8_t* report = (uint8_t *)& mouse_report;

	int8_t xMotion = gyro_x_output/10;
	int8_t yMotion = gyro_y_output/10;
	int8_t zMotion = acc_z_output;

	if (abs(xMotion) < 5){
		xMotion = 0;
	}
	if (abs(yMotion) < 5){
		yMotion = 0;
	}

	if( (abs(zMotion) > 122) && (abs(xMotion) < 5) && (abs(yMotion) < 5) ){
		tap = 1;
		xMotion = 0;
		yMotion = 0;
	}


	sprintf((char*)tx_buffer, " %ld, %ld \r\n",
										  xMotion,yMotion);
	tx_com(tx_buffer, strlen((char const*)tx_buffer));//this is for uart only




	/*usb mouse*/
/*
			//left//
			if(acc_y_output > 200 ) //&& acc_x_output > -50 && acc_y_output < 250 && acc_y_output > -150 )
			{
		     xMotion = -10;				// mouse x 	//
		     yMotion = 0;				// mouse y //
			}
			//right//
			else if(acc_y_output < 100) //&& acc_x_output > -50 && acc_y_output < 400 && acc_y_output > -250 )
			{
			 xMotion = 10;				// mouse x 	//
			 yMotion = 0;				// mouse y //
			}
			//up//
			else if(acc_z_output > -150) //&& acc_x_output > -400 && acc_y_output < 150 && acc_y_output > 0 )
			{
			 xMotion = 0;				// mouse x 	//
			 yMotion = -10;				// mouse y //
			}
				//down//
			else if(acc_z_output < -400) //&& acc_x_output > -450 && acc_y_output < 50 && acc_y_output > -150 )
			{
			 xMotion = 0;				// mouse x 	//
			 yMotion = 10;				// mouse y //

			}
				//stop//
			else if(acc_x_output < 200 && acc_x_output > -200 && acc_y_output < 200 && acc_y_output > -200)
			{
			 xMotion = 0;				// mouse x 	//
			 yMotion = 0;				// mouse y //
			}*/
	report[0] = leftClick;
//	report[0] = leftClick || tap;
	report[0] |= rightClick << 1;
	report[0] |= 0 << 2;
	report[1] = xMotion;
	report[2] = yMotion;
	report[3] = 0;

//	if (tap) {
//		HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_1);
//	}

	HIDS_Update_Char(REPORT_CHAR_UUID,
						0,
						0,
						sizeof(mouse_report_t),
						(uint8_t *)& mouse_report);
}

static void HIDSAPP_Scrolling_UpdateChar(void)
{
	HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_1);
	mouse_report_t mouse_report;
	uint8_t* report = (uint8_t *)& mouse_report;

	int8_t scrollMotion = gyro_y_output/10;

	report[0] = 0;
	report[0] |= 0 << 1;
	report[0] |= 0 << 2;
	report[1] = 0;
	report[2] = 0;
	report[3] = scrollMotion;

	HIDS_Update_Char(REPORT_CHAR_UUID,
						0,
						0,
						sizeof(mouse_report_t),
						(uint8_t *)& mouse_report);
}

static void HIDSAPP_Change_SW1(void)
{
	if (HAL_GetTick() - lastSW1Change < HIDSAPP_DEBOUNCE_THRESHOLD) {
		return;
	}

	HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_5);
	leftClick = !leftClick;
	lastSW1Change = HAL_GetTick();
}

static void HIDSAPP_Change_SW2(void)
{
	if (HAL_GetTick() - lastSW2Change < HIDSAPP_DEBOUNCE_THRESHOLD) {
		return;
	}

	middleButtonInfo.isPressed = !middleButtonInfo.isPressed;
	if (middleButtonInfo.isPressed) {
		middleButtonInfo.pressedTimestamp = HAL_GetTick();
		// If button was pressed shortly after a previous press (i.e. double click), switch between tracking and scrolling
		if (middleButtonInfo.pressedTimestamp - middleButtonInfo.releasedTimestamp < HIDSAPP_SINGLE_CLICK_THRESHOLD) {
			if (wasScrolling) {
				HIDSAPP_Change_Tracking();
			} else {
				if (isTracking) {
					HIDSAPP_Change_Tracking();
				}
				HIDSAPP_Change_Scrolling();
			}
		}
		// Otherwise, if we were scrolling, deactivate scrolling
		else if (isScrolling) {
			HIDSAPP_Change_Scrolling();
			wasScrolling = 1;
		}
		// Otherwise, deactivate or activate tracking
		else {
			HIDSAPP_Change_Tracking();
			wasScrolling = 0;
		}
	} else {
		middleButtonInfo.releasedTimestamp = HAL_GetTick();
	}

	lastSW2Change = HAL_GetTick();
}

static void HIDSAPP_Change_SW3(void)
{
	if (HAL_GetTick() - lastSW3Change < HIDSAPP_DEBOUNCE_THRESHOLD) {
		return;
	}

	HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_1);
	rightClick = !rightClick;
	lastSW3Change = HAL_GetTick();
}
/* USER CODE END FD */

/*************************************************************
 *
 * LOCAL FUNCTIONS
 *
 *************************************************************/
/* USER CODE BEGIN FD_LOCAL_FUNCTIONS*/
static void HIDSAPP_Change_Tracking(void)
{
	isTracking = !isTracking;
	if (isTracking) {
		HW_TS_Start(HIDSAPP_Context.TimerTracking_Id, HIDSAPP_TRACKING_INTERVAL);
	} else {
		HW_TS_Stop(HIDSAPP_Context.TimerTracking_Id);
	}
}

static void HIDSAPP_Change_Scrolling(void)
{
	isScrolling = !isScrolling;
	if (isScrolling) {
		HW_TS_Start(HIDSAPP_Context.TimerScrolling_Id, HIDSAPP_SCROLLING_INTERVAL);
	} else {
		HW_TS_Stop(HIDSAPP_Context.TimerScrolling_Id);
	}
}
/* USER CODE END FD_LOCAL_FUNCTIONS*/

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
